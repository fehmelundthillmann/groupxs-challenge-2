$dates = [
    {id: 1, color: "blue", timeStart: "09:00", timeEnd: "15:30"},
    {id: 2, color: "green", timeStart: "09:00", timeEnd: "14:30"},
    {id: 3, color: "brown", timeStart: "09:30", timeEnd: "11:30"},
    {id: 4, color: "red", timeStart: "11:30", timeEnd: "12:00"},
    {id: 5, color: "orange", timeStart: "14:30", timeEnd: "15:00"},
    {id: 6, color: "yellow", timeStart: "15:30", timeEnd: "16:00"}
];

$(document).ready(function () {
    initializeCalendarHeader();
    var weDonotWorkBeforeHour = 6;
    var weDonotWorkAfterHour = 18;
    initializeCalendarBody(weDonotWorkBeforeHour, weDonotWorkAfterHour);
    addDates();
});

function initializeCalendarBody(minHour, maxHour) {
    for (var i = (minHour - 1); i < maxHour; i++) {
        var emptyDay = "<div class='timeslot_row' data-time='" + (i+1) + "'></div>";
        $(".timeslots_container").append(emptyDay);
    }
}

function initializeCalendarHeader() {
    var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
    var currDate = new Date();
    $(".date_number_container").text(currDate.getDate());
    $(".date_name_container").text(weekday[currDate.getDay()]);
}

function addDates() {
    var datesCloned = $dates.slice(),
        conflicts = calculateConflicts($dates);
    $.each($dates, function(key, date) {
        datesCloned.splice($.inArray(date, datesCloned),1);
        addDate(date.color, date.timeStart, date.timeEnd, date.id, datesCloned, conflicts);
    });
}

function addDate(color, timeStart, timeEnd, id, otherDates, conflicts) {
    var explodedStartTime = timeStart.split(":"),
        startHour = parseInt(explodedStartTime[0]),
        startMinutes = parseInt(explodedStartTime[1]),
        explodedEndTime = timeEnd.split(":"),
        endHour = parseInt(explodedEndTime[0]),
        endMinutes = parseInt(explodedEndTime[1]),
        lineHeight = $(".timeslot_row").outerHeight(),
        firstHour = 6;

    var startPercentage = startMinutes / 60,
        endPercentage = endMinutes / 60,
        height = ((endHour + endPercentage) - (startHour + startPercentage)) * lineHeight,
        startPosition = (startHour - (firstHour-1) + startPercentage) * lineHeight,
        width = getWidthForAppointment(id, conflicts),
        leftPosition = getLeftForAppointment(id, conflicts);

    createNewElement(width, height, startPosition, leftPosition, color);
}

function getLeftForAppointment(id, conflicts) {
    var localConflicts = [];
    $.each(conflicts, function (key, conflictingPair) {
        if (conflictingPair[0] === id || conflictingPair[1] === id) {
            localConflicts.push(conflictingPair);
        }
    });
    if (localConflicts.length > 0) {
        var pointsLeft = 0,
            currDate = getDateById(id);
        $.each(localConflicts, function (key, conflictingPair) {
            var date = getDateById(conflictingPair[0]);
            if (conflictingPair[0] === id) {
                date = getDateById(conflictingPair[1]);
            }
            if (checkLargerAppointment(currDate, date)) {
                pointsLeft++;
            }
        });
        var fullWidth = $(".timeslots_container").width();
        return (fullWidth / (localConflicts.length + 1)) * pointsLeft;
    }
    return 0;
}

function checkLargerAppointment(currDate, date) {
    var explodedStartTime = currDate.timeStart.split(":"),
        startHour = parseInt(explodedStartTime[0]),
        startMinutes = parseInt(explodedStartTime[1]),
        explodedEndTime = currDate.timeEnd.split(":"),
        endHour = parseInt(explodedEndTime[0]),
        endMinutes = parseInt(explodedEndTime[1]),
        explodedOtherStartTime = date.timeStart.split(":"),
        otherStartHour = parseInt(explodedOtherStartTime[0]),
        otherStartMinutes = parseInt(explodedOtherStartTime[1]),
        explodedOtherEndTime = date.timeEnd.split(":"),
        otherEndHour = parseInt(explodedOtherEndTime[0]),
        otherEndMinutes = parseInt(explodedOtherEndTime[1]),
        start = startHour + (startMinutes / 60),
        end = endHour + (endMinutes / 60),
        otherStart = otherStartHour + (otherStartMinutes / 60),
        otherEnd = otherEndHour + (otherEndMinutes / 60),
        duration = end - start,
        otherDuration = otherEnd - otherStart;

    if (otherDuration > duration) {
        return true;
    }
    return false;
}

function getDateById(id) {
    var returnableDate = false;
    $.each($dates, function(key, date) {
        if (date.id === id) {
            returnableDate = date;
        }
    });
    return returnableDate;
}

function getWidthForAppointment(id, conflicts) {
    var localConflicts = 0;
    $.each(conflicts, function (key, conflictingPair) {
        if (conflictingPair[0] === id || conflictingPair[1] === id) {
            localConflicts++;
        }
    });
    if (localConflicts > 0) {
        return 100 / localConflicts;
    }
    return 100;
}

function calculateConflicts(dates) {
    var otherDates = dates.slice(),
        conflicts = [];
    $.each(dates, function (key, date) {
        var explodedStartTime = date.timeStart.split(":"),
            startHour = parseInt(explodedStartTime[0]),
            startMinutes = parseInt(explodedStartTime[1]),
            startPercentage = startMinutes / 60,
            explodedEndTime = date.timeEnd.split(":"),
            endHour = parseInt(explodedEndTime[0]),
            endMinutes = parseInt(explodedEndTime[1]),
            endPercentage = endMinutes / 60,
            end = endHour + endPercentage,
            start = startHour + startPercentage;
        $.each(otherDates, function (key, otherDate) {
            var explodedOtherStartTime = otherDate.timeStart.split(":"),
                otherStartHour = parseInt(explodedOtherStartTime[0]),
                otherStartMinutes = parseInt(explodedOtherStartTime[1]),
                otherStartPercentage = otherStartMinutes / 60,
                explodedOtherEndTime = otherDate.timeEnd.split(":"),
                otherEndHour = parseInt(explodedOtherEndTime[0]),
                otherEndMinutes = parseInt(explodedOtherEndTime[1]),
                otherEndPercentage = otherEndMinutes / 60,
                otherStart = otherStartHour + otherStartPercentage,
                otherEnd = otherEndHour + otherEndPercentage;

            if ((start >= otherStart && start < otherEnd && end > otherStart && end < otherEnd) || (start < otherStart && start < otherEnd && end > otherStart && end > otherEnd) || (start < otherStart && start < otherEnd && end > otherStart && end < otherEnd)) {
                var minorId = date.id,
                    majorId = otherDate.id;
                if (majorId < minorId) {
                    var swp = minorId;
                    minorId = majorId;
                    majorId = swp;
                }
                var conflictingPair = [minorId, majorId];
                if (majorId !== minorId) {
                    var inArray = false;
                    for(var i = 0; i < conflicts.length; i++) {
                        if(conflicts[i][0] == conflictingPair[0] && conflicts[i][1] == conflictingPair[1]) {
                            inArray = true;
                        }
                    }
                    if (!inArray) {
                        conflicts.push(conflictingPair);
                    }
                }
            }
        });
    });
    return conflicts;
}

function createNewElement(widthPercent, height, startPosition, leftPosition, color) {
    var element = $("<div class='timeslot'></div>");
    element.css('background-color', color);
    element.css('height', height + 'px');
    element.css('width', widthPercent + '%');
    element.css('top', startPosition + 'px');
    element.css('left', leftPosition + 'px');
    $(".timeslots_container").append(element);
}